import yaml
import git
import shutil
import os
import re
import tarfile
import sys

def replace_data(file_path, target, new_value):
    with open(file_path, 'r') as yaml_file:
        data = yaml.safe_load(yaml_file)
    
    keys = target.split('.')
    nested_dict = data
    
    for key in keys[:-1]:
        nested_dict = nested_dict[key]

    nested_dict[keys[-1]] = new_value

    with open(file_path, 'w') as yaml_file:
        yaml.dump(data, yaml_file)

def increase_version(version, component):
    major, minor, patch = map(int, version.split('.'))
    
    # Increment the specified component
    if component == 'major':
        major += 1
    elif component == 'minor':
        minor += 1
    elif component == 'patch':
        patch += 1
    else:
        patch += 1

    return f"{major}.{minor}.{patch}"

def compare_versions(v1, v2):
    v1_major, v1_minor, v1_patch = map(int, v1.split('.'))
    v2_major, v2_minor, v2_patch = map(int, v2.split('.'))
    
    if v1_major != v2_major:
        return v1_major - v2_major
    elif v1_minor != v2_minor:
        return v1_minor - v2_minor
    else:
        return v1_patch - v2_patch

def extract_chart(repo_url, local_repo_path, local_extract_path, chart_name):
    if not os.path.exists(local_repo_path):
        git.Repo.clone_from(repo_url, local_repo_path)
    else:
        # Pull latest changes
        repo = git.Repo(local_repo_path)
        repo.remotes.origin.pull()


    tgz_files = [f for f in os.listdir(local_repo_path) if f.endswith('.tgz')]

    # Extract version numbers from filenames
    versions = []
    for filename in tgz_files:
        version_match = re.search(r'{}-(\d+\.\d+\.\d+)\.tgz'.format(chart_name), filename)
        if version_match:
            versions.append(version_match.group(1))

    component = 'patch' if len(sys.argv) == 1 else sys.argv[2]
    new_tag = 'v1' if len(sys.argv) == 1 else sys.argv[1]

    # Find the latest version
    latest_version = max(versions, key=lambda x: tuple(map(int, x.split('.'))))

    # Get the filename for the latest version
    latest_filename = f'{chart_name}-{latest_version}.tgz'

    with tarfile.open(os.path.join(local_repo_path, latest_filename), 'r:gz') as tar:
        tar.extractall(local_extract_path)

    # replace new docker images
    replace_data('{}/{}/values.yaml'.format(local_extract_path, chart_name), 'image.repositories.backend.tag', new_tag)

    # specify next version
    with open('{}/{}/next_version.txt'.format(local_extract_path, chart_name), 'w') as f:
        f.write(increase_version(latest_version, component))

#------------------------main --------------------------------
repo_url = 'git@github.com:duongdx-kma/helm-repository.git'
local_repo_path = '/tmp/helm/repo'
local_extract_path = '/tmp/helm/chart'
chart_name = 'socket_chart'
extract_chart(repo_url, local_repo_path, local_extract_path, chart_name)
