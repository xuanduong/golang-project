# Golang Restful API

# up backend container
docker run -d --name golang_backend -p 8088:8088 \
-e DB_DRIVER=mysql \
-e DB_HOST=172.17.0.2 \
-e DB_USER=duongdx \
-e DB_PASSWORD=duong98 \
-e DB_DATABASE=db_business \
-e DB_PORT=3306 \
-e JWT_SECRET=123456789abc \
-e APP_PORT=8088 \
-e APP_ENV=dev \
-e AWS_REGION='' \
-e SECRET_MANAGER_KEY='' \
duong1200798/golang_backend /bin/bash

# up mysql container
docker run -d --name database \
-e MYSQL_USER=duongdx \
-e MYSQL_PASSWORD=duong98 \
-e MYSQL_ROOT_PASSWORD=rootpassword \
-e MYSQL_DATABASE=db_business \
 duong1200798/socket_app_mysql:v1 

# login testing 
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"user_name":"duongdx","password":"password"}' \
  http://localhost:8088/login